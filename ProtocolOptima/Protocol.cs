﻿using ProtocolOptima.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ProtocolOptima
{
   public class Protocol
   {
        #region Client
        public static byte[] FormPrefixByte(string localIpAddress, byte localAddress, string remoteIpAddress, byte remoteAddress, CommandType cipher)
        {
            byte[] comand = new byte[38];
            comand[0] = 3;
            comand[1] = 2;
            comand[2] = 1;
            comand[3] = 0;

            byte[] localIp_byte = Encoding.ASCII.GetBytes(localIpAddress);
            byte[] remoteIp_byte = Encoding.ASCII.GetBytes(remoteIpAddress);

            Array.Copy(localIp_byte, 0, comand, 4, 15);
         
            comand[19] = localAddress;
            Array.Copy(remoteIp_byte, 0, comand, 20, 15);


            comand[35] = remoteAddress;
            if (cipher == CommandType.OFF)
            { 
                comand[36] = 21;
                comand[37] = 9;
            }
            if (cipher == CommandType.ON)
            {
                comand[36] = 20;
                comand[37] = 9;
            }

            if (cipher == CommandType.POLL)
            {
                comand[36] = 22;
                comand[37] = 0;
            }

            if (cipher == CommandType.ON_OFF_universal)
            {
                comand[36] = 23;
                comand[37] = 13;
            }

            return comand;
        }

        public static string FormPrefixString(string localIpAddress, byte localAddress, string remoteIpAddress, byte remoteAddress, CommandType cipher)
        {
            byte[] comand = FormPrefixByte(localIpAddress, localAddress, remoteIpAddress, remoteAddress, cipher);

            return BitConverter.ToString(comand);
        }

        public static byte[] On_Off_Command(byte[] prefix ,
            bool GPS_L1, bool GPS_L2, bool GPS_L3,
            bool GLONASS_L1, bool GLONASS_L2, bool GLONASS_L3,
            bool Galileo_L1, bool Galileo_L2, bool Galileo_L3,
            bool Beidou_L1, bool Beidou_L2, bool Beidou_L3,
            byte FreqPC)
        {
            byte[] comand = new byte[51];
            Array.Copy(prefix, 0, comand, 0, prefix.Length);
            comand[38] = (byte)(GPS_L1 == true ? 1 : 0);
            comand[39] = (byte)(GPS_L2 == true ? 1 : 0);
            comand[40] = (byte)(GLONASS_L1 == true ? 1 : 0);
            comand[41] = (byte)(GLONASS_L2 == true ? 1 : 0);
            comand[42] = (byte)(Galileo_L1 == true ? 1 : 0);
            comand[43] = (byte)(Galileo_L2 == true ? 1 : 0);
            comand[44] = (byte)(Beidou_L1 == true ? 1 : 0);
            comand[45] = (byte)(Beidou_L2 == true ? 1 : 0);
            if (FreqPC >= 0 && FreqPC <= 2)
            comand[46] = FreqPC;
            else comand[46] = 2;
            comand[47] = (byte)(GPS_L3 == true ? 1 : 0);
            comand[48] = (byte)(GLONASS_L3 == true ? 1 : 0);
            comand[49] = (byte)(Galileo_L3 == true ? 1 : 0);
            comand[50] = (byte)(Beidou_L3 == true ? 1 : 0);
            return comand;
        }

        public static string On_Off_CommandString(byte[] prefix,
            bool GPS_L1, bool GPS_L2, bool GPS_L3,
            bool GLONASS_L1, bool GLONASS_L2, bool GLONASS_L3,
            bool Galileo_L1, bool Galileo_L2, bool Galileo_L3,
            bool Beidou_L1, bool Beidou_L2, bool Beidou_L3,
            byte FreqPC)
        {
            string comand = BitConverter.ToString(On_Off_Command(prefix, 
                GPS_L1, GPS_L2, GPS_L3,
                GLONASS_L1, GLONASS_L2, GLONASS_L3, 
                Galileo_L1, Galileo_L2, GLONASS_L3, 
                Beidou_L1, Beidou_L2, Beidou_L3, 
                FreqPC));
           
            return comand;
        }

        public static byte[] Poll_State_Command(string localIpAddress, byte localAddress, string remoteIpAddress, byte remoteAddress, CommandType cipher)
        {
            byte[] comand = FormPrefixByte(localIpAddress,localAddress,remoteIpAddress,remoteAddress,cipher);
            return comand;
        }

        public static string Poll_State_CommandString(string localIpAddress, byte localAddress, string remoteIpAddress, byte remoteAddress, CommandType cipher)
        {
            string comand = FormPrefixString(localIpAddress, localAddress, remoteIpAddress, remoteAddress, cipher);
            return comand;
        }

        public static StationCodegram DecryptionCodegram(byte[] codegram)
        {
            StationCodegram stationCodegram = new StationCodegram();
            byte[] ipAddress = new byte[16];
            Array.Copy(codegram, 4, ipAddress, 0, 15);

            stationCodegram.localIpAddress = Encoding.ASCII.GetString(ipAddress);
            stationCodegram.localAddress = codegram[19];

            Array.Copy(codegram, 20, ipAddress, 0, 15);

            stationCodegram.remoteIpAddress = Encoding.ASCII.GetString(ipAddress);
            stationCodegram.remoteAddress = codegram[35];

            if (codegram[36] == 20)
                stationCodegram.cipher = CommandType.ON;
            if (codegram[36] == 21)
                stationCodegram.cipher = CommandType.OFF;
            if (codegram[36] == 23)
                stationCodegram.cipher = CommandType.ON_OFF_universal;

            stationCodegram.GPS_L1 = (codegram[38] == 1 ? true : false);
            stationCodegram.GPS_L2 = (codegram[39] == 1 ? true : false);
            stationCodegram.GLONASS_L1 = (codegram[40] == 1 ? true : false);
            stationCodegram.GLONASS_L2 = (codegram[41] == 1 ? true : false);
            stationCodegram.Galileo_L1 = (codegram[42] == 1 ? true : false);
            stationCodegram.Galileo_L2 = (codegram[43] == 1 ? true : false);
            stationCodegram.Beidou_L1 = (codegram[44] == 1 ? true : false);
            stationCodegram.Beidou_L2 = (codegram[45] == 1 ? true : false);
            stationCodegram.Existance_L1 = (codegram[46] == 1 ? true : false);
            stationCodegram.Existance_L2 = (codegram[47] == 1 ? true : false);
            stationCodegram.FreqPC = codegram[48];
            stationCodegram.SignalLevel = codegram[49];

            stationCodegram.GPS_L3 = (codegram[50] == 1 ? true : false);
            stationCodegram.GLONASS_L3 = (codegram[51] == 1 ? true : false);
            stationCodegram.Galileo_L3 = (codegram[52] == 1 ? true : false);
            stationCodegram.Beidou_L3 = (codegram[53] == 1 ? true : false);
            stationCodegram.Existance_L3 = (codegram[54] == 1 ? true : false);
            stationCodegram.IsRemouteControl = (codegram[55] == 1 ? true : false);
            return stationCodegram;
        }


        public static StationCodegram DecryptionCodegram(string codegram)
        {
            StationCodegram stationCodegram = new StationCodegram();

            stationCodegram.localIpAddress = codegram.Substring(4,15);
            stationCodegram.localAddress = Convert.ToByte(codegram[19]);

            stationCodegram.remoteIpAddress = codegram.Substring(20, 15);
            stationCodegram.remoteAddress = Convert.ToByte(codegram[35]);

            if (codegram[36] == 20)
                stationCodegram.cipher = CommandType.ON;
            if (codegram[36] == 21)
                stationCodegram.cipher = CommandType.OFF;
            if (codegram[36] == 23)
                stationCodegram.cipher = CommandType.ON_OFF_universal;

            stationCodegram.GPS_L1 = (codegram[38] == 1 ? true : false);
            stationCodegram.GPS_L2 = (codegram[39] == 1 ? true : false);
            stationCodegram.GLONASS_L1 = (codegram[40] == 1 ? true : false);
            stationCodegram.GLONASS_L2 = (codegram[41] == 1 ? true : false);
            stationCodegram.Galileo_L1 = (codegram[42] == 1 ? true : false);
            stationCodegram.Galileo_L2 = (codegram[43] == 1 ? true : false);
            stationCodegram.Beidou_L1 = (codegram[44] == 1 ? true : false);
            stationCodegram.Beidou_L2 = (codegram[45] == 1 ? true : false);
            stationCodegram.Existance_L1 = (codegram[46] == 1 ? true : false);
            stationCodegram.Existance_L2 = (codegram[47] == 1 ? true : false);
            stationCodegram.FreqPC = Convert.ToByte(codegram[48]);
            stationCodegram.SignalLevel = Convert.ToByte(codegram[49]);

            stationCodegram.GPS_L3 = (codegram[50] == 1 ? true : false);
            stationCodegram.GLONASS_L3 = (codegram[51] == 1 ? true : false);
            stationCodegram.Galileo_L3 = (codegram[52] == 1 ? true : false);
            stationCodegram.Beidou_L3 = (codegram[53] == 1 ? true : false);
            stationCodegram.Existance_L3 = (codegram[54] == 1 ? true : false);
            stationCodegram.IsRemouteControl = (codegram[55] == 1 ? true : false);

            return stationCodegram;
        }

        #endregion

        #region Server
     
        public static byte[] FormAnswer(byte[] prefix,
            bool GPS_L1, bool GPS_L2, bool GPS_L3,
            bool GLONASS_L1, bool GLONASS_L2, bool GLONASS_L3,
            bool Galileo_L1, bool Galileo_L2, bool Galileo_L3,
            bool Beidou_L1, bool Beidou_L2, bool Beidou_L3,
            byte FreqPC)
        {

            byte[] comand = new byte[56];
            Array.Copy(prefix, 0, comand, 0, prefix.Length);
            comand[38] = (byte)(GPS_L1 == true ? 1 : 0);
            comand[39] = (byte)(GPS_L2 == true ? 1 : 0);
            comand[40] = (byte)(GLONASS_L1 == true ? 1 : 0);
            comand[41] = (byte)(GLONASS_L2 == true ? 1 : 0);
            comand[42] = (byte)(Galileo_L1 == true ? 1 : 0);
            comand[43] = (byte)(Galileo_L2 == true ? 1 : 0);
            comand[44] = (byte)(Beidou_L1 == true ? 1 : 0);
            comand[45] = (byte)(Beidou_L2 == true ? 1 : 0);
            comand[46] = 1;
            comand[47] = 1;
            if (FreqPC >= 0 && FreqPC <= 2)
                comand[48] = FreqPC;
            else comand[48] = 2;
            comand[49] = 30;
            comand[50] = (byte)(GPS_L3 == true ? 1 : 0);
            comand[51] = (byte)(GLONASS_L3 == true ? 1 : 0);
            comand[52] = (byte)(Galileo_L3 == true ? 1 : 0);
            comand[53] = (byte)(Beidou_L3 == true ? 1 : 0);

            comand[54] = 1;
            comand[55] = 1;
            return comand;
        }


        public static StationCodegram DecryptionCodegramFromClient(byte[] codegram)
        {        
            StationCodegram stationCodegram = new StationCodegram();
            byte[] ipAddress = new byte[16];
            Array.Copy(codegram, 4, ipAddress, 0, 15);

            stationCodegram.localIpAddress = Encoding.ASCII.GetString(ipAddress);
            stationCodegram.localAddress = codegram[19];

            Array.Copy(codegram, 20, ipAddress, 0, 15);

            stationCodegram.remoteIpAddress = Encoding.ASCII.GetString(ipAddress);
            stationCodegram.remoteAddress = codegram[35];

            if (codegram[36] == 20)
                stationCodegram.cipher = CommandType.ON;
            if (codegram[36] == 21)
                stationCodegram.cipher = CommandType.OFF;
            if (codegram[36] == 23)
                stationCodegram.cipher = CommandType.OFF;

            if (codegram.Length < 40)
                return stationCodegram;


            stationCodegram.GPS_L1 = (codegram[38] == 1 ? true : false);
            stationCodegram.GPS_L2 = (codegram[39] == 1 ? true : false);
            stationCodegram.GLONASS_L1 = (codegram[40] == 1 ? true : false);
            stationCodegram.GLONASS_L2 = (codegram[41] == 1 ? true : false);
            stationCodegram.Galileo_L1 = (codegram[42] == 1 ? true : false);
            stationCodegram.Galileo_L2 = (codegram[43] == 1 ? true : false);
            stationCodegram.Beidou_L1 = (codegram[44] == 1 ? true : false);
            stationCodegram.Beidou_L2 = (codegram[45] == 1 ? true : false);
            stationCodegram.FreqPC = codegram[46];
            stationCodegram.GPS_L3 = (codegram[47] == 1 ? true : false);
            stationCodegram.GLONASS_L3 = (codegram[48] == 1 ? true : false);
            stationCodegram.Galileo_L3 = (codegram[49] == 1 ? true : false);
            stationCodegram.Beidou_L3 = (codegram[50] == 1 ? true : false);
            return stationCodegram;
        }
        #endregion



    }


}
