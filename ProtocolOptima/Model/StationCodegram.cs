﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProtocolOptima.Model
{
    public struct StationCodegram
    {
        public string localIpAddress;
        public byte localAddress;
        public string remoteIpAddress;
        public byte remoteAddress;
        public CommandType cipher;
        public bool GPS_L1;
        public bool GPS_L2;
        public bool GLONASS_L1;
        public bool GLONASS_L2;
        public bool Galileo_L1;
        public bool Galileo_L2;
        public bool Beidou_L1;
        public bool Beidou_L2;
        public bool Existance_L1;
        public bool Existance_L2;
        public byte FreqPC;
        public byte SignalLevel;
        public bool GPS_L3;
        public bool GLONASS_L3;
        public bool Galileo_L3;
        public bool Beidou_L3;
        public bool Existance_L3;
        public bool IsRemouteControl;
    }
}
