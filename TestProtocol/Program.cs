﻿using System;
using System.Text;

namespace TestProtocol
{
    class Program
    {
        static void Main(string[] args)
        {

            byte[] prefix = ProtocolOptima.Protocol.FormPrefixByte("037.214.072.080", 1, "037.214.072.019" ,2, ProtocolOptima.CommandType.ON);
            byte[] command = new byte[56];
            byte[] form = ProtocolOptima.Protocol.On_Off_Command(prefix, true, true, true, true, false, true, false, true, true, false, false, false, 1);
            for (int i = 0; i <45 ; i++)
                command[i] = form[i];
            command[46] = 0;
            command[47] = 0;
            command[48] = 2;
            command[49] = 4;
            command[50] = form[47];
            command[51] = form[48];
            command[52] = form[49];
            command[53] = form[50];
            command[54] = 1;
            command[55] = 1;



            Console.WriteLine(Encoding.ASCII.GetString(command) + "\n");

            ProtocolOptima.Model.StationCodegram sc = ProtocolOptima.Protocol.DecryptionCodegram(command);

            Console.WriteLine("localIpAddress " + sc.localIpAddress);
            Console.WriteLine("localAddress " + sc.localAddress);
            Console.WriteLine("remoteIpAddress " + sc.remoteIpAddress);
            Console.WriteLine("remoteAddress " + sc.remoteAddress);
            Console.WriteLine("cipher " + sc.cipher.ToString());
            Console.WriteLine("GPS_L1 " + sc.GPS_L1);
            Console.WriteLine("GPS_L2 " + sc.GPS_L2);
            Console.WriteLine("GLONASS_L1 " + sc.GLONASS_L1);
            Console.WriteLine("GLONASS_L2 " + sc.GLONASS_L2);
            Console.WriteLine("Galileo_L1 " + sc.Galileo_L1);
            Console.WriteLine("Galileo_L2 " + sc.Galileo_L2);
            Console.WriteLine("Beidou_L1 " + sc.Beidou_L1);
            Console.WriteLine("Beidou_L2 " + sc.Beidou_L2);
            Console.WriteLine("Existance_L1 " + sc.Existance_L1);
            Console.WriteLine("Existance_L2 " + sc.Existance_L2);
            Console.WriteLine("FreqPC " + sc.FreqPC);
            Console.WriteLine("SignalLevel " + sc.SignalLevel);
            Console.WriteLine("GPS_L3 " + sc.GPS_L3);
            Console.WriteLine("GLONASS_L3 " + sc.GLONASS_L3);
            Console.WriteLine("Galileo_L3 " + sc.Galileo_L3);
            Console.WriteLine("Beidou_L3 " + sc.Beidou_L3);
            Console.WriteLine("Existance_L3 " + sc.Existance_L3);
            Console.WriteLine("IsRemouteControl " + sc.IsRemouteControl);

            Console.ReadLine();
        }
    }

    public enum CommandType
    {
        ON,
        OFF,
        POLL
    }

}
